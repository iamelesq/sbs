package main

import (
	"net/http"

	"github.com/justinas/nosurf"
)

// creates a handler hoding a per page cookie. The cookie is
// equipped with CSRF protection on all POST requests.
func NoSurf(next http.Handler) http.Handler {
	csrfHandler := nosurf.New(next)
	csrfHandler.SetBaseCookie(http.Cookie{
		HttpOnly: true,
		Path:     "/",
		Secure:   app.IsProduction,
		SameSite: http.SameSiteLaxMode,
	})

	return csrfHandler
}

// adds per request load and save of session capabilities.
func SessionLoad(next http.Handler) http.Handler {
	return session.LoadAndSave((next))
}
