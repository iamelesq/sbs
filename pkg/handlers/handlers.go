package handlers

import (
	"net/http"

	"github.com/elesq/sbs/pkg/config"
	"github.com/elesq/sbs/pkg/models"
	"github.com/elesq/sbs/pkg/render"
)

var Repo *Repository

type Repository struct {
	App *config.AppConfig
}

// Takes an appConfig and creates a new repository structure which
// acts as a wrapper for the appconfig accessible as App.
func NewRepo(a *config.AppConfig) *Repository {
	return &Repository{
		App: a,
	}
}

// Accepts a repository and sets the package repository Repo as that
// which was passed in.
func NewHandlers(repo *Repository) {
	Repo = repo
}

// homepage handler
func (repo *Repository) Home(w http.ResponseWriter, r *http.Request) {
	remoteIP := r.RemoteAddr
	repo.App.Session.Put(r.Context(), "remote_ip", remoteIP)
	render.RenderTemplate(w, "home.page.tmpl", &models.TemplateData{})
}

// about page handler
func (repo *Repository) About(w http.ResponseWriter, r *http.Request) {
	stringMap := make(map[string]string)
	stringMap["test"] = "hello there"

	remoteIP := repo.App.Session.GetString(r.Context(), "remote_ip")
	stringMap["remote_ip"] = remoteIP

	render.RenderTemplate(w, "about.page.tmpl", &models.TemplateData{
		StringMap: stringMap,
	})
}
