package config

import (
	"html/template"
	"log"

	"github.com/alexedwards/scs/v2"
)

// holds the application's config
type AppConfig struct {
	UseCache      bool
	TemplateCache map[string]*template.Template
	InfoLog       *log.Logger
	IsProduction  bool
	Session       *scs.SessionManager
}
