package render

import (
	"bytes"
	"html/template"
	"log"
	"net/http"
	"path/filepath"

	"github.com/elesq/sbs/pkg/config"
	"github.com/elesq/sbs/pkg/models"
)

var app *config.AppConfig

// sets the config for the template package
func NewTemplates(a *config.AppConfig) {
	app = a
}

// creates a cache of all page files and for each matches on any
// associated template files for layout required to support the
// page building routine. Pages are added to a cache structure
// that allows for easy lookup at code level over the calls to
// parse and build on each request.
func CreateTemplateCache() (map[string]*template.Template, error) {
	cache := map[string]*template.Template{}

	// get all pages files. *page.tmpl from templates
	pages, err := filepath.Glob("./templates/*.page.tmpl")
	if err != nil {
		return cache, err
	}

	// range through pages
	for _, page := range pages {
		name := filepath.Base(page)
		templateSet, err := template.New(name).ParseFiles(page)
		if err != nil {
			return cache, err
		}

		// get layouts required to build page
		matches, err := filepath.Glob("./templates/*.layout.tmpl")
		if err != nil {
			return cache, err
		}

		if len(matches) > 0 {
			templateSet, err = templateSet.ParseGlob("./templates/*.layout.tmpl")
			if err != nil {
				return cache, err
			}

		}

		// add to the cache
		cache[name] = templateSet
	}

	return cache, nil
}

func AddDefaultData(td *models.TemplateData) *models.TemplateData {
	return td
}

// renders templates using a templates caching strategy. The renderer pulls the
// template from the app config cache and creates a bytes buffer in which to
// execute the  template. If the execution is error free then the template is
// written to the http.ResponseWriter.
func RenderTemplate(w http.ResponseWriter, tmpl string, data *models.TemplateData) {

	var templateCache map[string]*template.Template

	if app.UseCache {
		templateCache = app.TemplateCache
	} else {
		templateCache, _ = CreateTemplateCache()
	}

	currentTemplate, ok := templateCache[tmpl]
	if !ok {
		log.Fatal("could not get template from the template cache")
	}

	// create bytes buffer and execute the template
	buf := new(bytes.Buffer)
	data = AddDefaultData(data)

	err := currentTemplate.Execute(buf, data)
	if err != nil {
		log.Println(err)
	}

	// render the template
	_, err = buf.WriteTo(w)
	if err != nil {
		log.Println(err)
	}

}
